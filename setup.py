import setuptools

with open("README.md", "r", encoding="utf-8") as fhand:
    long_description = fhand.read()

setuptools.setup(
    name="vcs-crawler",
    version="0.0.1",
    author="xakiv",
    author_email="cbenhabib.dev@gmail.com",
    description="LeFilament VCS Crawler, in python.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/xakiv/vsc-crawler",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "aiohttp==3.8.6",
        "black==23.11.0",
        "isort==5.12.0",
        "requests==2.31.0",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.8",
    entry_points={
        "console_scripts": [
            "vcs-crawler = vcs_crawler.cli:main",
        ]
    },
)
