List LeFilament's VCS public repositories

## Installation & Usage

```
python3 -m venv venv
source venv/bin/activate
pip install git+https://gitlab.com/xakiv/vcs-crawler.git
vcs-crawler 
```

Results are stored in `data/` subdirectory

Alternatively, it is possible to call vcs_crawler as python module:

```commandline
source venv/bin/activate
python
>>> from vcs_crawler import process
>>> dataset = process()
>>> print(dataset.as_dict)
```

## More information

```
usage: cli.py [-h] [--mode {async,sync,beta}]

List LeFilament's VCS public repositories.

options:
  -h, --help            show this help message and exit
  --mode {async,sync,beta}
                        Choose which mode (default async) is used to handle API requests.


```