import json
import time
from pathlib import Path


def write_results(data, mode):
    wd = Path.cwd().joinpath("data/")
    wd.mkdir(parents=True, exist_ok=True)
    epoch_time = int(time.time())
    res = wd.joinpath(f"results_{epoch_time}_{mode}.json")
    with res.open("w") as opened:
        json.dump(data, opened)
