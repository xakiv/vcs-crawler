import time


def time_it(func):
    def inner():
        start = time.perf_counter()
        func()
        stop = time.perf_counter()
        print(f"Done in {stop - start:0.4f} seconds")

    return inner
