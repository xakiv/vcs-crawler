class VCSCrawlerError(Exception):
    pass


class RemoteServerError(VCSCrawlerError):
    def __init__(self, message="Remote server is unavailable. "):
        self.message = message
        super().__init__(self.message)


class NoMorePageAvailable(VCSCrawlerError):
    def __init__(self, message="No data remains. "):
        self.message = message
        super().__init__(self.message)
