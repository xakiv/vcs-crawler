import logging
import unittest

import requests

from .crawler import process
from .crawler import LIST_GROUPS_URL, LIST_PROJECTS_URL

logger = logging.getLogger(__name__)

logger.setLevel(logging.INFO)


def get_total_objects(url):
    total_objects = 0
    try:
        response = requests.get(url)
        total_objects = int(response.headers.get("X-Total", "0"))
    except (requests.exceptions.RequestException, ValueError):
        pass
    return total_objects


class TestVCSCrawler(unittest.TestCase):
    async_dataset = None
    sync_dataset = None

    @classmethod
    def setUpClass(cls):
        cls.async_dataset = process(mode="async", _write_results=False)
        cls.sync_dataset = process(mode="sync", _write_results=False)

    def test_vcs_crawler_projects(self):
        total_projects = get_total_objects(LIST_PROJECTS_URL)
        async_projects_names = list(
            map(lambda x: x["name"], TestVCSCrawler.async_dataset.projects)
        )
        sync_projects_names = list(
            map(lambda x: x["name"], TestVCSCrawler.sync_dataset.projects)
        )
        self.assertEqual(set(async_projects_names), set(sync_projects_names))
        self.assertEqual(
            len(TestVCSCrawler.async_dataset.projects),
            len(TestVCSCrawler.sync_dataset.projects),
        )
        self.assertEqual(len(TestVCSCrawler.async_dataset.projects), total_projects)

    def test_vcs_crawler_groups(self):
        total_groups = get_total_objects(LIST_GROUPS_URL)
        self.assertEqual(
            len(TestVCSCrawler.async_dataset.groups), len(self.sync_dataset.groups)
        )
        self.assertEqual(len(TestVCSCrawler.async_dataset.groups), total_groups)


if __name__ == "__main__":
    unittest.main()
