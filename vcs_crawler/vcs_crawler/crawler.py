import asyncio
import logging
import aiohttp
import requests

from .model import Dataset
from .writer import write_results

VCS_API_URL = "https://sources.le-filament.com/api/v4/"
LIST_PROJECTS_URL = f"{VCS_API_URL}projects/"
LIST_GROUPS_URL = f"{VCS_API_URL}groups/"
PER_PAGE = "100"

logging.basicConfig(level=logging.INFO, format="%(message)s")
logger = logging.getLogger(__name__)


class PageCrawler:
    def __init__(self):
        self.page = 0
        self.next_page = 2

    def __iter__(self):
        return self

    def __next__(self):
        if not self.next_page:
            raise StopIteration
        self.page += 1
        return self


class SyncAPIClient:
    def __init__(self):
        self.dataset = Dataset()

    def get_page(self, url, page):
        try:
            response = requests.get(url, params={"page": page, "per_page": PER_PAGE})
            data = response.json()
            next_page = response.headers.get("X-Next-Page", None)
        except requests.exceptions.RequestException:
            data, next_page = None, None
        return data, next_page

    def get_projects(self, url, dataset):
        for crawler in PageCrawler():
            data, crawler.next_page = self.get_page(url, crawler.page)
            dataset.append_projects(data)
            logger.debug(dataset.as_dict)

    def get_groups(self, url, dataset):
        for crawler in PageCrawler():
            groups, crawler.next_page = self.get_page(url, crawler.page)

            for group in groups:
                group_id = group.get("id")
                group_name = group.get("name")
                related_dataset = Dataset()

                self.get_projects(
                    f"{LIST_GROUPS_URL}{group_id}/projects",
                    related_dataset,
                )
                self.get_groups(
                    f"{LIST_GROUPS_URL}{group_id}/subgroups",
                    related_dataset,
                )
                dataset.append_groups(group_name, related_dataset)
                logger.info(f"group {group_name} done")

    def process(self):
        self.get_projects(LIST_PROJECTS_URL, self.dataset)
        self.get_groups(LIST_GROUPS_URL, self.dataset)
        return self.dataset


class AsyncAPIClient:
    def __init__(self):
        self.dataset = Dataset()
        self.session = None

    async def handshake(self, page_info):
        for _, row in page_info.items():
            try:
                async with self.session.get(
                    row["url"], params={"per_page": PER_PAGE}
                ) as response:
                    row["total_pages"] = int(response.headers.get("X-Total-Pages", "1"))
                    if response.status == 200:
                        row["data"] = await response.json()
            except aiohttp.ClientConnectorError:
                continue
        return page_info

    async def handle_groups(self, group, dataset):
        group_id = group.get("id")
        group_name = group.get("name")
        related_dataset = Dataset()
        await self.inflate_dataset(
            related_dataset,
            f"{LIST_GROUPS_URL}{group_id}/projects",
            f"{LIST_GROUPS_URL}{group_id}/subgroups",
        )

        dataset.append_groups(group_name, related_dataset)
        logger.info(f"group {group_name} done")

    async def inflate_dataset(self, dataset, project_url, group_url):
        page_info = {
            "groups": {"url": group_url, "total_pages": 0, "data": []},
            "projects": {"url": project_url, "total_pages": 0, "data": []},
        }
        page_info = await self.handshake(page_info)
        projects_data = page_info["projects"]["data"]
        groups_data = page_info["groups"]["data"]
        projects_total_pages = page_info["projects"]["total_pages"]
        groups_total_pages = page_info["groups"]["total_pages"]
        logger.debug(projects_data)

        if projects_data:
            dataset.append_projects(projects_data)
        if projects_total_pages > 1:
            for page in range(2, projects_total_pages + 1):
                async with self.session.get(
                    project_url, params={"per_page": PER_PAGE, "page": page}
                ) as response:
                    data = await response.json()
                    dataset.append_projects(data)
        if groups_data and groups_total_pages == 1:
            for group in groups_data:
                await self.handle_groups(group, dataset)
        if groups_total_pages > 1:
            for page in range(2, groups_total_pages + 1):
                async with self.session.get(
                    project_url, params={"per_page": PER_PAGE, "page": page}
                ) as response:
                    data = await response.json()
                    for group in data:
                        await self.handle_groups(group, dataset)

    async def runner(self):
        async with aiohttp.ClientSession() as self.session:
            await self.inflate_dataset(self.dataset, LIST_PROJECTS_URL, LIST_GROUPS_URL)

    def process(self):
        asyncio.run(self.runner())
        return self.dataset


def process(mode="async", _write_results=False):
    dataset = Dataset()
    if mode == "async":
        dataset = AsyncAPIClient().process()
    elif mode == "sync":
        dataset = SyncAPIClient().process()
    if _write_results:
        write_results(dataset.as_dict, mode)
    return dataset
