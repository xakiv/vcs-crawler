from dataclasses import asdict, dataclass, field


@dataclass
class Dataset:
    groups: list[dict] = field(default_factory=list)
    projects: list[dict] = field(default_factory=list)

    @staticmethod
    def remap_project(row):
        return {
            "name": row.get("name"),
            "url": row.get("http_url_to_repo"),
            "branch": row.get("default_branch"),
        }

    def append_projects(self, data):
        self.projects += [Dataset.remap_project(row) for row in data]

    def append_groups(self, group_name, subgroup):
        self.groups += [
            {
                group_name: {
                    "groups": subgroup.groups,
                    "projects": subgroup.projects,
                }
            }
        ]

    @property
    def as_dict(self):
        return asdict(self)
