import argparse

from vcs_crawler import process
from vcs_crawler import time_it


@time_it
def main():
    parser = argparse.ArgumentParser(
        description="List LeFilament's VCS public repositories. "
    )
    parser.add_argument(
        "--mode",
        choices=["async", "sync"],
        default="async",
        help="Choose which mode is used to handle API requests. ",
    )
    args = parser.parse_args()
    process(args.mode, _write_results=True)


if __name__ == "__main__":
    main()
